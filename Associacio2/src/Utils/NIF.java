/*
 * Classe NIF, que ens permet tant validar si un NIF es correcte, com calcular la lletra de un NIF.
 * @author Ferran
 * data de creació: 29/04/2019
 */

//Package
package Utils;


//Imports
import java.util.HashMap;


public class NIF {
    public static boolean validarNIF(String nif) {

        

        boolean NIFCorrecte = false;
        boolean vuit_numeros = false;
        char[] charArray = nif.toCharArray();
        
        if(charArray.length == 9){
            for(int i =0; i<charArray.length-1; i++){
                char c2 = charArray[i];
                if(c2 >= 48 && c2 <= 57){
                    vuit_numeros = true;
                }else{
                    vuit_numeros=false;        
                }
            }
            
            if(vuit_numeros){
                char lletra = nif.charAt(nif.length()-1);
                lletra = Character.toUpperCase(lletra);
                int numero = Integer.parseInt(nif.substring(0, nif.length()-1));
                if(lletra == calcularLletraNIF(numero)){
                   NIFCorrecte=true; 
                }
                
            }
        }
        
        return NIFCorrecte;
    }  
    
    
    
    
    public static char calcularLletraNIF(int nif){
        
        //http://www.interior.gob.es/web/servicios-al-ciudadano/dni/calculo-del-digito-de-control-del-nif-nie
        
        HashMap<Integer,Character> mapalletres = new HashMap<>();
        String lletres = "TRWAGMYFPDXBNJZSQVHLCKE";
        char[] lletresChar = lletres.toCharArray();
        
        for(int i = 0; i<23; i++){
            mapalletres.put(i, lletresChar[i]);
        }
        
        int resto = nif % 23;
        
        char lletra = mapalletres.get(resto);
        
        return lletra;
    }
}
