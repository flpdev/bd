/*
 * Classe Persona
 * @author Ferran
 * data de creació: 29/04/2019
 */

//Package
package associacio2.POJO;



public class Persona {
    protected String nom;
    protected String cognom;
    protected String NIF;
    
    
    //Constructor
    public Persona(String nom, String cognom, String NIF){
        this.nom = nom;
        this.cognom = cognom;
        this.NIF = NIF;
    }

    
    
    //GETTERS I SETTERS
    public String getNom() {
        return nom;
    }

    public String getCognom() {
        return cognom;
    }

    public String getNIF() {
        return NIF;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
    //toString
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("Nom: ").append(nom).append(" Cognom: ").append(cognom).append(" NIF: ").append(NIF);

        return sb.toString();
    }
    
    
}
