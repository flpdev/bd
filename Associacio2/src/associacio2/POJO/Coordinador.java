/*
 * Classe Coordinador
 * @author Ferran
 * data de creació: 29/04/2019
 */

//Package
package associacio2.POJO;


public class Coordinador extends Persona {
    private int sou;

    //Constructors
    public Coordinador(String nom, String cognom, String NIF) {
        super(nom, cognom, NIF);
    }
    
    public Coordinador(Persona persona, int sou) {
        super(persona.getNom(), persona.getCognom(), persona.getNIF());
        this.sou = sou;
    }
    
    //SETTERS
    public void setSou(int sou) {
        this.sou = sou;
    }
    
    
    //toString
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append(" Coordinador --> ").append(super.toString()).append(" Sou: ").append(sou);

        return sb.toString();
    }

    
    
    
}
