/*
 * Classe Soci
 * @author Ferran
 * data de creació: 29/04/2019
 */

//Package
package associacio2.POJO;


public class Soci extends Persona{
    private int quota;

    //Constructors
    public Soci(int quota, String nom, String cognom, String NIF) {
        super(nom, cognom, NIF);
        this.quota = quota;
    }
    
    public Soci(Persona persona, int quota) {
        super(persona.getNom(), persona.getCognom(), persona.getNIF());
        this.quota = quota;
    }
    
    
    //toString
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append(" Soci --> ").append(super.toString()).append(" Quota: ").append(quota);

        return sb.toString();
    }
}
