/*
 * Classe Projecte
 * @author Ferran
 * data de creació: 29/04/2019
 */

//Package
package associacio2.POJO;


//Imports
import com.db4o.collections.ArrayList4;


public class Projecte {
    private String nom;
    private Coordinador coordinador;
    private ArrayList4<Soci> socis;
    
    
    //Contructor
    public Projecte(Coordinador c, String nom){
        this.coordinador = c;
        this.nom = nom;
        this.socis = new ArrayList4<>();
       
    }
    
    
    /*
     * Funció afegirSoci
     * rep cop a parametre una instancia de la classe Soci
     * afegeix el objecte passat com a parametre al arrayList4 de socis
    */
    public void afegirSoci(Soci soci){
        this.socis.add(soci);
    }
    
    
    /*
     * Funció deleteSoci
     * rep cop a parametre una instancia de la classe Soci
     * elimina el objecte passat com a parametre del arrayList4 de socis
    */
    public void deleteSoci(Soci soci){
        for(int i = 0; i<socis.size(); i++){
            if(socis.get(i).getNIF().equals(soci.getNIF())){
                socis.remove(i);
            }
        }
    }
    
    
    //SETTERS
    public void setCoordinador(Coordinador c){
        this.coordinador = c;
    }
    
    public Coordinador getCoordinador(){
        return this.coordinador;
    }
    
    public ArrayList4<Soci> getSocis(){
        return this.socis;
    }
    
    public void setNom(String nom){
        this.nom=nom;
    }
    
    
    //toString
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        
        sb.append("Projecte --> ").append(" Nom: ").append(nom).append(" Coordinador: "); 
        
        if(coordinador!= null){
            sb.append(coordinador.getNom()).append( " Numero de socis: ").append(socis.size());
        }else{
            sb.append("null").append( " Numero de socis: ").append(socis.size());
        }
        
        
        return sb.toString();
    }
    
    
    /*
     * Funció toStringSocis
     * no rep cap parametre
     * mostra les dades del projecte i dels seus socis
    */
    public String toStringSocis(){
        StringBuilder sb = new StringBuilder();
        
        sb.append("Projecte --> ").append(" Nom: ").append(nom).append(" Coordinador: "); 
        
        //Si s'ha eliminat la persona coordinadora del projecte mostrem el camp coodinador com null
        if(coordinador!= null){
            sb.append(coordinador.getNom()).append( " Numero de socis: ").append(socis.size());
        }else{
            sb.append("null").append( " Numero de socis: ").append(socis.size());
        }
        
        //Si hi ha socis es mostren, sino es mostra un missatge de que no hi han socis
        if(!socis.isEmpty()){
            sb.append("\n").append("SOCIS: ").append("\n");
            for(Soci s: socis){
                sb.append(" Soci: ").append(" Nom: ").append(s.getNom()).append(" Cognom: ").append(s.getCognom()).append(" NIF:").append(s.getNIF()).append("\n");
            }
         
        }else{
            sb.append("\n").append("NO TE SOCIS");
        }
        
        return sb.toString();
    }
}
