/*
 * Associacio2.java. Aquest programa ens permet gestionar la base de dades d'una assosciació
 * @author Ferran
 * data de creació: 29/04/2019
 */

//Package
package associacio2;


//imports
import static Utils.NIF.validarNIF;
import associacio2.POJO.Coordinador;
import associacio2.POJO.Persona;
import associacio2.POJO.Projecte;
import associacio2.POJO.Soci;
import java.util.ArrayList;
import java.util.Scanner;



public class Associacio2 {

    private static BaseDades db;

    
    /*
     * Funció main
     * rep arguments com a parametre
     * executa la logica principal del programa
    */
    public static void main(String[] args) {
        
        int opcio; 
        System.out.println("------- MENU --------");
        
        do{
            
            //demanem la opcio al usuari
            opcio = menu();
            
            //Instanciem la base de dades
            db = new BaseDades("Associacio.db");
            
            //executem la opcio escollida per l'usuari
            switch(opcio){
                case 1:
                    mostrarPersones();
                    
                    break;
                    
                case 2:
                    mostrarProjectes();
                    
                    break;
                case 3:
                    nouSoci();
                    
                    break;
                    
                case 4:
                    promocionarSoci();
                    
                    break;
                    
                case 5:
                    nouProjecte();
                    
                    break;
                    
                case 6:
                    eliminarPersona();
                   
                    break;
                    
                case 7:
                    eliminarProjecte();
                    
                    break;
                    
                case 8:
                    modificarProjecte();
                    
                    break;
                default:
                    db.closeDB();
                    break;

            }
            
            if(opcio != 0){
                System.out.println("------- MENU --------");
            }
            
            
            //tanquem la conexio a la base de dades
            db.closeDB();
            
        }while(opcio != 0);

    }
    
    /*Funcio menu, que no rep cap parametre i retorna un int, 
     en funcio de la opcio escollida per el usuari */
    public static int menu(){
        int opcio = -1; 
        
        System.out.println("1.- Veure persones");
        System.out.println("2.- Veure projectes");
        System.out.println("3.- Nou soci");
        System.out.println("4.- Promoció soci");
        System.out.println("5.- Nou projecte");
        System.out.println("6.- Eliminar persona");
        System.out.println("7.- Eliminar projecte");
        System.out.println("8.- Modificar projecte");
        System.out.println("0.- Sortir");
        System.out.println("Opcio: ");
        opcio = new Scanner(System.in).nextInt();
        
        return opcio;
    }


    /*
     * Funció mostrarPersones
     * no rep cap parametre
     * mostra per consola les persones guardades a la base de dades
    */
    public static void mostrarPersones(){
        ArrayList<Persona> persones = db.getPersones();

        //Si no hi ha cap persona mostrem missatge d'error
        if(persones.isEmpty()){
            System.out.println("No hi ha cap persona al sistema");
            
        //Si hi ha persones les mostrem
        }else{
            System.out.println("------- PERSONES --------");
            for (Persona p : persones) {
                
                System.out.print("Persona --> ");
                
                //Depenent del tipus de la subclasse de persona cridem un toString o un altre
                if(p instanceof Soci){
                    System.out.println(((Soci)p).toString()); 
                }else if(p instanceof Coordinador){
                    System.out.println(((Coordinador)p).toString()); 
                }               
            }
            
        }
        
    }
    
    
    /*
     * Funció mostrarProjectes
     * no rep cap parametre
     * mostra per consola els projectes guardats a la base de dades
    */
    public static void mostrarProjectes(){
        ArrayList<Projecte> projectes =  db.getProjectes();
        
        //Si no hi ha cap projecte mostrem missatge d'error
        //Si hi han projectes els mostrem per pantalla
        if(!projectes.isEmpty()){
            System.out.println("------- PROJECTES --------");
            for(Projecte p: projectes){
                System.out.println(p);
            }
        }else{
            System.out.println("No hi ha cap projecte al sistema");
        }
        
    }

    
    /*
     * Funció nouSoci
     * no rep cap parametre
     * crea un nou soci i el guarda a la base de dades
    */
    public static void nouSoci(){
        
        System.out.println("------- NOU SOCI --------");
        
        //demanem les dades del nou soci al usuari
        String nom = demanarNom();
        String cognom = demanarCognom();
        String NIF = demanarNIF();
        int quota = demanarQuota();

        //Mirem si exixteix una persona ja amb aquest nif, si no existeix la guardem, si existeix mostrem un missatge d'error
        Persona p = db.getPersonaByNIF(NIF);
        if(p == null){
            db.insertSoci(new Soci(quota, nom, cognom, NIF));
        }else{
            System.out.println("Ja existeix una persona amb aquest NIF");
        }
        
        
    }

    
    /*
     * Funció promocionarSoci
     * no rep cap parametre
     * promociona un soci a coordinador
    */
    public static void promocionarSoci(){
        System.out.println("------- PERSONES --------");
        
        //demanem el nif del soci a promocionar
        String NIF = demanarNIF();
        
        //Mirem si exixteix una persona ja amb aquest nif
        Persona p = db.getPersonaByNIF(NIF);
        
        //Si existeix mirem si es soci o coordinador
        if(p != null){
             
            //Si es soci demanem el sou i el convertim en coordinador
            if(p instanceof Soci){
                
                int sou = demanarSou();
                Coordinador c = new Coordinador((Persona)p, sou);
                db.insertCoordinador(c);
                
                //Eliminem el Soci de la BD
                db.eliminarPersona(p);
                System.out.println("Soci promocionat correctament");
                
            //Si es coordinador mostrem un missatge d'error
            }else{
                System.out.println("Ja es coordinador");
            }
         //Si no hi ha cap persona amb aquest nif mostrem un missatge d'error   
        }else{
            System.out.println("No existeix cap soci amb aquest NIF");
        }
    }

    
    /*
     * Funció nouProjecte
     * no rep cap parametre
     * crea un nou projecte i el guarda a la base de dades
    */
    public static void nouProjecte(){
        System.out.println("------- NOU PROJECTE --------");
       
        //demanem el nif del coodinador
        String NIF = demanarNIF();
        Persona p = db.getPersonaByNIF(NIF);
        
        //Si exixteix i es coordinador
        if(p != null){
            
            if(p instanceof Coordinador){
                
                //Demanem el nom del projecte i mirem si ja existeix algun projecte amb aquest nom
                String nom = demanarNom();
                Projecte proj = db.getProjecteByNom(nom);
                
                //Si no existeix cap projecte amb aquest nom el guardem a la base de dades
                if(proj == null){
                    Projecte projecte = new Projecte((Coordinador)p, nom);
                    db.insertProjecte(projecte);
                    System.out.println("Projecte creat correctament");
                
                //Si ja hi ha un projecte amb aquest nom mostrem un missatge d'error
                }else{
                    System.out.println("Ja existeix un projecte amb aquest nom");
                }
            //Si la persona no es coordinador mostrem un missatge d'error  
            }else{
                System.out.println("Un soci no pot coordinar un projecte");
            }
            
        //Si no exixteix cap persona amb aquest nif mostrem un missatge d'error
        }else{
            System.out.println("No existeix cap persona amb aquest NIF");
        }
    }

    
    /*
     * Funció eliminarPersona
     * no rep cap parametre
     * elimina una persona de la base de dades
    */
    public static void  eliminarPersona(){
        System.out.println("------- ELIMINAR PERSONA --------");
        
        //Demanem el nif de la persona a eliminar
        String NIF = demanarNIF();
        
        //Mirem si existeix alguna persona amb aquest nif
        Persona persona = db.getPersonaByNIF(NIF);
        
        //Si exixtex
        if(persona != null){
            
            //Mirem si es coordinador
            if(persona instanceof Coordinador){
                
                
                    //Mostrem els projectes que te el coordinador
                    ArrayList<Projecte> projectes = db.getProjectes();
                    System.out.println("El coordinador te associats els seguents projectes: ");
                    for(Projecte p: projectes){
                            if(p.getCoordinador() != null){
                                
                                if(p.getCoordinador().getNIF().equals(NIF)){
                                    System.out.println(p);
                                }
                            }
                        }
                    
                    //Demanem permis per eliminar el coordinador
                    char permis = demanarConfirmacio("Segur que vols eliminar al coordinador? Y / N: ");
                    
                    //Si el usuari ens dona permis eliminem el coordinador dels seus projectes
                    if(permis == 'Y'){
                        if(!projectes.isEmpty()){
                        for(Projecte p: projectes){
                            if(p.getCoordinador() != null){
                                System.out.println("El coordinador te associats els seguents projectes: ");
                                if(p.getCoordinador().getNIF().equals(NIF)){
                                    System.out.println(p);
                                    p.setCoordinador(null);
                                    db.insertProjecte(p);
                                }
                            }
                        }
                    }
                        //Eliminem el coordinador de la base de dades
                        db.eliminarPersona(persona);
                        System.out.println("Persona eliminada");
                        
                    //Si l'usuari no ens dona permis mostrem el seguent missatge
                    }else{
                        System.out.println("Operacio cancelada per el usuari");  
                    }
                    
            //Si la persona es un soci, no cal demanar permis
            }else{
                
                //Eliminem el soci dels projectes on estigues registrat
                ArrayList<Projecte> projectes = db.getProjectes();
                
                if(!projectes.isEmpty()){
                    for(Projecte p: projectes){
                        if(!p.getSocis().isEmpty()){
                            p.deleteSoci((Soci)persona);
                            db.insertProjecte(p);
                        }
                    }
                }
                
                //Eliminem el soci de la base de dades
                db.eliminarPersona(persona);
                System.out.println("Persona eliminada");
            }
            
        //Si no existeix cap persona amb aquest nif mostre un missatge d'error
        }else{
            System.out.println("No existeix cap persona amb aquest NIF");
        }
        
        
    }
    
    /*
     * Funció eliminarProjecte
     * no rep cap parametre
     * elimina un projecte de la base de dades
    */
    public static void eliminarProjecte(){
        System.out.println("------- ELIMINAR PROJECTE --------");
        
        //Demanem el nom del projecte que es vol eliminar
        String nomProjecte = demanarNom();
        
        //Mirem si existeix un projecte amb aquest nom
        Projecte p = db.getProjecteByNom(nomProjecte);
        
        //Si existeix
        if(p != null){
            
            //Mostrem les dades del projecte
            System.out.println("RESUM DEL PROJECTE");
            System.out.println(p.toStringSocis());
            
            //Demanem permis al usuari per eliminar el projecte
            char permis = demanarConfirmacio("Segur que vols eliminar el projecte? Y / N: ");
            
            //Si ems dona permis el borrem de la base de dades
            if(permis == 'Y'){
                db.eliminarProjecte(p);
                System.out.println("Projecte eliminat");  
            
            //Si no ens dona permis mostrem el seguent missatge
            }else{
                System.out.println("Operacio cancelada per el usuari");  
            }
            
        //Si no hi ha cap projecte amb el nom introduit mostrem un missatge d'error            
        }else{
           System.out.println("No existeix cap projecte amb aquest nom"); 
        }
    }
    
    
    /*
     * Funció modificarProjecte
     * no rep cap parametre
     * permet modificar un projecte ja guardat a la base de dades
    */
    public static void modificarProjecte(){
        System.out.println("------- MODIFICAR PROJECTE --------");
        int opcio2;
        
        //demanem el nom del projecte a modificar
        String nomProjecte = demanarNom();
        
        //Mirem si hi ha algun projecte a la base de dades amb aquest nom
        Projecte p = db.getProjecteByNom(nomProjecte);
        
        //Si existeix 
        if(p!=null){
            
            //Mostrem un submenu perque el usuari esculli una accio
            do{
                opcio2 = menuModificacio();

                switch(opcio2){
                    case 1:
                        modificarCoordinador(p);
                        break;
                    case 2:
                        afegirComponent(p);
                        break;
                    case 3:
                        eliminarComponent(p);
                        break;
                    default:
                        db.insertProjecte(p);
                }
            }while(opcio2 != 0);
        
        //Si no es troba cap projecte amb el nom introduit mostrem un misstge d'error
        }else{
            System.out.println("No existeix cap projecte amb aquest nom"); 
        }
        
    }
    
    
    /*
     * Funció menuModificacio
     * no rep cap parametre
     * mostra un submenu amb les opcions per modificar un projecte
     * retorna un int en funcio de la opcio escollida
    */
    public static int menuModificacio(){
        
        int opcio2 = -1;
        
        System.out.println("1.- Canviar coordinador");
        System.out.println("2.- Afegir component");
        System.out.println("3.- Eliminar component");
        System.out.println("0.- Guardar canvis");
        System.out.println("Opcio: ");
        opcio2 = new Scanner(System.in).nextInt();
        
        return opcio2;
    }
    
    
    /*
     * Funció modificarProjecte
     * rep el projecte a modificar per parametre
     * modifica el coordinador del projecte
    */
    public static void modificarCoordinador(Projecte p){
        
        //demanem el nif del coordinador que volem assosciar al projecte
        String NIF = demanarNIF();
        
        //Mirem si existeix alguna persona amb aquest nif
        Persona persona = db.getPersonaByNIF(NIF);
        
        //si existeix mirem si es tracta d'un coordinador
        if(persona != null){
            //Si es un coordinador, l'associem al projecte i tornem a guardar el projecte a la base de dades
            if(persona instanceof Coordinador){
                p.setCoordinador((Coordinador)persona);
                db.insertProjecte(p);
                System.out.println("Coordinador canviat correctament");
                
            //Si es un soci, mostrem el seguent missatge
            }else{
                System.out.println("Un soci no pot coordinar un projecte");
            }
        
        //Si no existeix cap persona amb aquest nif mostrem un missatge d'error
        }else{
            System.out.println("No existeix cap persona amb aquest NIF");
        }
        
    }
    
    
    /*
     * Funció afegirComponent
     * rep el projecte a modificar per parametre
     * afegeix un component a un projecte
    */
    public static void afegirComponent(Projecte p){
        
        //demanem el nif de la persona que volem afegir
        String NIF = demanarNIF();
        
        //Mirem si existeix alguna persona amb aquet nif
        Persona persona = db.getPersonaByNIF(NIF);
        
        //si existeix mirem si es un soci
        if(persona != null){
            
            //Si es soci l'afegim al projecte
            if(persona instanceof Soci){
                p.afegirSoci((Soci)persona);               
                db.insertProjecte(p);
                System.out.println("Component afegit correctament");
                
            //Si no es soci mostrem un missatge d'error
            }else{
                System.out.println("Un coordinador no pot ser soci d'un projecte");
            }
        
        //Si no trobem cap persona amb aquest nif mostrem un missatge d'error
        }else{
            System.out.println("No existeix cap persona amb aquest NIF"); 
        }
        
    }
    
    
    /*
     * Funció eliminarComponent
     * rep el projecte a modificar per parametre
     * elimina un component d'un projecte
    */
    public static void eliminarComponent(Projecte p){
        
        //demanem el nif de la persona que volem eliminar
        String NIF = demanarNIF();
        
        //Mirem si existeix alguna persona amb aquet nif
        Persona persona = db.getPersonaByNIF(NIF);
        
        //si existeix mirem si es un soci
        if(persona != null){
            
            //Si es soci l'eliminem del projecte
            if(persona instanceof Soci){
                p.deleteSoci((Soci)persona);
                db.insertProjecte(p);
                System.out.println("Component eliminat correctament");
                
            //Si no es soci mostrem un missatge d'error
            }else{
                System.out.println("Un coordinador no pot ser soci d'un projecte");
            }
            
        //Si no trobem cap persona amb aquest nif mostrem un missatge d'error
        }else{
            System.out.println("No existeix cap persona amb aquest NIF"); 
        }
    }
    
    
    
    //FUNCIONS PER DEMANAR DADES
    
    /*
     * Aquestes funcions demanen al usuari una dada, fan les comprovacions corresponents
     * i segueixen demanan la dada fins que el valor introduit sigui correcte
    */
    
    public static String demanarNom(){

        String nom;

        do{
            System.out.println("Nom: ");
            nom = new Scanner(System.in).nextLine();

            if(nom.equals("")){
                System.out.println("S'ha d'introduir un nom");
            }
        }while(nom.equals(""));

        return nom;
    }

    public static String demanarCognom(){

        String cognom;

        do{
            System.out.println("Cognom: ");
            cognom = new Scanner(System.in).nextLine();

            
            if(cognom.equals("")){
                System.out.println("S'ha d'introduir un cognom");
            }
        }while(cognom.equals(""));

        return cognom;
    }

    public static String demanarNIF(){

        String NIF;
        boolean nifValid = false;
        do{
            System.out.println("NIF: ");
            NIF = new Scanner(System.in).nextLine();
            
            //Mirem que el nif tingui 8 numeros, 1 caracter i que el caracter sigui correcte
            nifValid = validarNIF(NIF);
            
            if(!nifValid){
                System.out.println("S'ha d'introduir un NIF correcte");
            }
        }while(!nifValid);

        return NIF;
    }

    public static int demanarQuota(){

        int quota;

        do{
            System.out.println("Quota: ");
            quota = new Scanner(System.in).nextInt();

            
            if(quota < 0){
                System.out.println("S'ha d'introduir una quota valida");
            }

        }while(quota < 0);

        return quota;
    }
    
    public static int demanarSou(){
        int sou;

        do{
            System.out.println("Sou: ");
            sou = new Scanner(System.in).nextInt();

            
            if(sou < 0){
                System.out.println("S'ha d'introduir un sou valida");
            }

        }while(sou < 0);

        return sou;
    }
    
    public static char demanarConfirmacio(String msg){
        char confirmacio;

        do{
            System.out.println(msg);
            confirmacio = new Scanner(System.in).next().charAt(0);
            confirmacio = Character.toUpperCase(confirmacio);
            
            if(confirmacio != 'Y' && confirmacio != 'N'){
                System.out.println("S'ha d'introduir una opció valida");
            }

        }while(confirmacio != 'Y' && confirmacio != 'N');

        return confirmacio;
    }
    
}
