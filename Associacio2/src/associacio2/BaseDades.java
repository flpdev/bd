/*
 * Classe BaseDades, que gestiona totes les operacions CRUD del nostre sistema de gestió d'associacions
 * @author Ferran
 * data de creació: 29/04/2019
 */

//package
package associacio2;

//Imports
import associacio2.POJO.Coordinador;
import associacio2.POJO.Persona;
import associacio2.POJO.Soci;
import associacio2.POJO.Projecte;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.util.ArrayList;




public class BaseDades{

    private final ObjectContainer db;
    
    //Constructor
    public BaseDades(String arxiu){
        this.db = Db4oEmbedded.openFile(arxiu);
    }
    
    

    // ------------- SELECTS ----------------------
    
    
    /*
     * Funció getPersones
     * no rep cap parametre
     * retorna totes les persones que hi ha a la base de dades
    */
    public ArrayList<Persona> getPersones(){
        ArrayList<Persona> persones = new ArrayList();
        
        //QueryByExample
        Persona p = new Persona(null, null,null);

        ObjectSet result = this.db.queryByExample(p);

        while(result.hasNext()){
           persones.add((Persona)result.next());     
        }
        return persones;
    }
    
    
    /*
     * Funció getProjectes
     * no rep cap parametre
     * retorna tots els projectes de la base de dades
    */
    public ArrayList<Projecte> getProjectes(){
        
        ArrayList<Projecte> projectes = new ArrayList<>();
        
        //QueryByExample
        Projecte p = new Projecte(null, null);
        ObjectSet result = db.queryByExample(p);
        
        while(result.hasNext()){
            Projecte p2 = (Projecte)result.next();
            projectes.add(p2);
        }
        return projectes;
    }
    
    
    /*
     * Funció getPersonaByNIF
     * rep un String per parametre, que es el nif de la persona a buscar
     * retorna la persona amb el nif passat per parametres, si no exiteix retorna null
    */
    public Persona getPersonaByNIF(String NIF){
        
        //QueryByExample
        Persona p = new Persona(null, null, NIF);
        Persona p2 = null;
        ObjectSet result = db.queryByExample(p);
        
        if(result.hasNext()){
            p2 =  (Persona)result.next();
        }
        
        return p2;
    }
    
    /*
     * Funció getProjecteByNom
     * rep un String per parametre, que es el nom del projecte a buscar
     * retorna el projecte amb el nom passat per parametres, si no exiteix retorna null
    */
    public Projecte getProjecteByNom(String nomProjecte){
        
        Projecte p = new Projecte(null, nomProjecte);
        //QueryByExample
        ObjectSet result = db.queryByExample(p);
        Projecte p2 = null;
        if(!result.isEmpty()){
            p2 = (Projecte)result.next();
        }
        
        return p2;
    }
    
    
    // ------------- INSERTS ---------------------
    
    
    /*
     * Funció insertCoordinador
     * rep un Coordinador per parametre, que es el coordinador a guardar a la base de dades
     * guarda el coodinador passat per parametre a la base de dades
     * no retorna res
    */
    public void insertCoordinador(Coordinador c){
        db.store(c);
    }
    
    
    /*
     * Funció insertSoci
     * rep un Soci per parametre, que es el soci a guardar a la base de dades
     * guarda el soci passat per parametre a la base de dades
     * no retorna res
    */
    public void insertSoci(Soci soci){
        db.store(soci);
    }
    
    
    /*
     * Funció insertProjecte
     * rep un projecte per parametre, que es el projecte a guardar a la base de dades
     * guarda el projecte passat per parametre a la base de dades
     * no retorna res
    */
    public void insertProjecte(Projecte p){
        db.store(p.getSocis());
        db.store(p);
    }
    
    
    
    // ------------- DELETES ---------------------
    
    
    /*
     * Funció insertProjecte
     * rep una Persona per parametre, que es la persona a borrar de la base de dades
     * borra la persona passada per parametre a la base de dades
     * no retorna res
    */
    public void eliminarPersona(Persona p){
        ObjectSet result = db.queryByExample(p);
       
       if(!result.isEmpty()){
           db.delete(p);
       }
    }
    
    /*
     * Funció insertProjecte
     * rep una Persona per parametre, que es la persona a borrar de la base de dades
     * borra la persona passada per parametre a la base de dades
     * no retorna res
    */
     public void eliminarProjecte(Projecte projecte){
        
        
        ObjectSet result = db.queryByExample(projecte);
        
        if(!result.isEmpty()){
            db.delete(projecte);
        }
    }
  
    // ------------- FUNCIONS EXTRA ---------------
    
    /*
    * Funcio closeDB, 
    * No rep cap parametre ni retorna res. 
    * Tanca la base de dades.
    */
    public void closeDB(){
        this.db.close();
    }
    
    
    
}

