/*
 * Classe comarca per treballar amb objectes complexos
 */
package comarques;

import com.db4o.collections.ArrayList4;

/**
 *
 * @author Ferran
 */
public class Comarca {
    private String nom; 
    private String capital; 
    private char provincia;
    private int altitut;
    private ArrayList4<Monument> monuments; 
    
    //Constructors
    
    public Comarca(){}

    public Comarca(String nom, String capital, char provincia, int altitut, ArrayList4<Monument> monuments) {
        this.nom = nom;
        this.capital = capital;
        this.provincia = provincia;
        this.altitut = altitut;
        if(monuments == null){
            this.monuments = new ArrayList4<>();
        }else{
            this.monuments = monuments;
        }
        
    }

    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public char getProvincia() {
        return provincia;
    }

    public void setProvincia(char provincia) {
        this.provincia = provincia;
    }

    public void setAltitut(int altitut) {
        this.altitut = altitut;
    }
    
    
    
    

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(); 
        sb.append(nom).append(", ").append(capital).append(", ").append(provincia)
                .append(" ").append(" altitud: ").append(altitut).append(" "); 
        
        if(monuments != null && monuments.size() > 0){
            for(Monument m: monuments){
                sb.append(m.toString());
            }
        }
        
        return sb.toString();
    }

    public ArrayList4<Monument> getMonuments() {
        return monuments;
    }
    
    
    
    
}
