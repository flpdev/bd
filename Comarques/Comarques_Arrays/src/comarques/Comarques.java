/*
 * Aquest programa ens permet fer operacions CRUD amb objectes complexos (Comarca, monument) a una base de dades DB40
 * Autor: Ferran Lopez Puig, DAM2T
 * Data de creacio: 5/3/2019
 */

//package
package comarques;

//imports
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.collections.ArrayList4;


public class Comarques {

    
    static ObjectContainer db;
    
    public static void main(String[] args) {
        
        //Obrim la conexio a la base de dades
        db = Db4oEmbedded.openFile("comarques.db");
        
        
        //EXERCICI 2
        Monument m1 = new Monument("Sagrada Familia", "SXIX");
        Monument m2 = new Monument("Monument 2", "SXXI");
        Monument m3 = new Monument("Monument 3", "SVI");
        
        ArrayList4<Monument> monuments1 = new ArrayList4<>();
        ArrayList4<Monument> monuments2 = new ArrayList4<>();
        
        monuments1.add(m1);
        monuments1.add(m2); 
        monuments2.add(m3);
        
        Comarca c1 = new Comarca("Barcelones", "Barcelona", 'B', 10, monuments1);
        
        
        //asociem la comarca als monuments
        for(Monument m: c1.getMonuments()){
            m.setComarca(c1);
        }
        
        
        //Guardem els objectes a la base de dades
        db.store(m1);
        db.store(m2);
        db.store(m3);
        db.store(c1);
        
        
        //Recuperem la comarca per comprovar que s'ha guardat amb el array de monuments
        Comarca c = new Comarca(null, null, '\0', 0, null);
        ObjectSet result = db.queryByExample(c); 
        
        if(!result.isEmpty()){
            Comarca c2 = (Comarca) result.next();
            System.out.println(c2);
        }
        
        //EXERCICI 3
        
        Comarca c3 = new Comarca(null, null, '\0', 0, null);
        ObjectSet result2 = db.queryByExample(c3); 
        
        if(!result2.isEmpty()){
            Comarca c4 = (Comarca) result2.next();
            c4.getMonuments().add(new Monument("monument afegit", "SXX"));
            System.out.println(c4);
        }
        
        
        
        
        //Comprovo si cal guardar la comarca quan nomes es guarden els monuments
        //Si es guarda la comarca dintre del monument, no cal guardar la comarca individualment
        Comarca c10 = new Comarca(null, null,'\0', 0, null);
        ObjectSet resultC10 = db.queryByExample(c10); 
        
        while(resultC10.hasNext()){
            Comarca c11 = (Comarca) resultC10.next(); 
            System.out.println(c11);
        }
        
        //Comprovo si cal guardar els monuments
        //Si es guarden dintre de la comarca, no cal guardarlos individualment.
        Monument m = new Monument(null, null);
        ObjectSet provaMonuments = db.queryByExample(m); 
        
        while(provaMonuments.hasNext()){
            Monument m4 = (Monument) provaMonuments.next();
            System.out.println(m4);
        }
        
        
        //tanquem la base dades
        db.close();
               
                

    } 
}
