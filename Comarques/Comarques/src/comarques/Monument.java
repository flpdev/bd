
/*
 * Classe monument per guardar dins d'una comarca
 */
package comarques;

/**
 *
 * @author Ferran
 */
public class Monument {
    private String nom; 
    private String segle;

    public Monument(String nom, String anyCreacio) {
        this.nom = nom;
        this.segle = anyCreacio;
    }

    public String getNom() {
        return nom;
    }

    public String getSegle() {
        return segle;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("Monument: ").append(nom).append(" Segle: ").append(segle);
        
        return sb.toString();
    }

    public void setSegle(String segle) {
        this.segle = segle;
    }
    
    
    
}
