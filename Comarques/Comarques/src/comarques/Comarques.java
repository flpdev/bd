/*
 * Aquest programa ens permet fer operacions CRUD amb objectes complexos (Comarca, monument)
 */
package comarques;
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
/**
 *
 * @author Ferran
 */
public class Comarques {

    /**
     * @param args the command line arguments
     */
    static ObjectContainer db;
    
    public static void main(String[] args) {
        
        //Obrim la conexio a la base de dades
        db = Db4oEmbedded.openFile("comarques.db");
        
        //Creem i guardem diferentes comarques amb el seu monument
        Comarca c1 = new Comarca("Barcelona", "Barcelona", 'B', 20, new Monument("Monument 1", "XV"));
        Comarca c2 = new Comarca ("Anoia", "Igualada", 'B', 434, new Monument("Monument 2", "XX"));
        Comarca c3 = new Comarca("Osona", "Vic", 'B', 523, new Monument("Monument 3", "XXl")); 
        Comarca c4 = new Comarca ("prova", "provaa", 'C', 4, new Monument("Monument 4", "XX"));
        
        
        db.store(c1);
        db.store(c2);
        db.store(c3);
        db.store(c4);
   
        
        
        
        System.out.println("Comarca "+ c1.getNom() + " afegida.");
        System.out.println("Comarca "+ c2.getNom() + " afegida.");
        System.out.println("Comarca "+ c3.getNom() + " afegida.");
   
        
        
        //EXERCICI 3
        Comarca c = new Comarca("Barcelona", "Barcelona", 'B', 20, new Monument("Monument 1", "XV"));
        ObjectSet result = db.queryByExample(c);
        
        if(!result.isEmpty()){
            Monument m = new Monument("Sagrada Familia", "XVlll");
            Comarca comarca = (Comarca) result.next();
            comarca.setMonument(m);
            db.store(comarca);
        }
        
        
        
        //Mostrem les comarques, els monuments, i les comarques amb monument d'un segle, per verificar que
        //les nostres funcions s'executen correctament
        System.out.println("---- COMARQUES ----");
        mostrarComarques();
         System.out.println("---- MONUMENTS ----");
        mostrarMonuments();
         System.out.println("---- COMARQUES SEGLE ----");
        mostrarComarquesSegle("XX");
        
        //EXERCICI 6
        
        Comarca c5 = new Comarca("Barcelona", "Barcelona", 'B', 20, new Monument("Sagrada Familia", "XVlll"));
        ObjectSet resultc5 = db.queryByExample(c5);
        
        if(!resultc5.isEmpty()){
            Comarca c6 = (Comarca) resultc5.next();
            c6.getMonument().setSegle("segle prova");
            db.store(c6);
        }
        
        //EXERCICI 6.2
        //Vam estar a classe mirant perque no em recuperava el objecte m de la base de dades
        //
        System.out.println("---- COMARQUES ----");
        mostrarComarques();
         System.out.println("---- MONUMENTS ----");
        mostrarMonuments();
        
        Monument m = new Monument("Sagrada Familia", "segle prova");
        ObjectSet result6 = db.queryByExample(m);
        
        if(!result6.isEmpty()){
            Monument m2 = (Monument) result6.next();
            
           //un cop recuperat el monument, busquem en quina comarca esta
           Comarca c7 = new Comarca(null, null, '\0', 0, m2);
           ObjectSet result7 = db.queryByExample(c7);
           
           if(!result7.isEmpty()){
               Comarca c8 = (Comarca) result7.next();
               c8.setAltitut(200);
               db.store(c8);
           }
        }
        
        //EXERCICI 7
        
        Monument m2 = new Monument("Monument 3", "XXl");
        ObjectSet result2 = db.queryByExample(m2);
        
        if(!result2.isEmpty()){
            Monument m3 = (Monument) result2.next();
            //System.out.println(m3); 
            
            db.delete(m3);         
        }
        
        //Problema -> El monument s'esborra pero dins del objecte comarca no.
        //EXERCICI 7.2
        Comarca com2 = new Comarca(null, null, '\0', 0, new Monument("Monument 4", "XX")); 
        ObjectSet resultCom2 = db.queryByExample(com2); 
        
        if(!resultCom2.isEmpty()){
            ObjectSet resultMon = db.queryByExample(((Comarca)resultCom2.next()).getMonument());
            
            if(!resultMon.isEmpty()){
                db.delete(resultMon.next());
            }
        }
        
        
        
        
        //Torno a mostrar els monumens per comprovar si s'han esborrat els monuments
        System.out.println("Monuments:");
        mostrarMonuments();
        System.out.println("Comarques:");
        mostrarComarques();
  
        db.close();
        
        
    }
    
    //Mostra totes les comarques de la base de dades
    public static void mostrarComarques(){
        
        Comarca c = new Comarca(null, null, '\0', 0, null);
        ObjectSet result = db.queryByExample(c);
        
        while(result.hasNext()){
            Comarca c2 = (Comarca) result.next();
            System.out.println(c2);
        }
    }
    
    
    //Mostra tots els monuments de la base de dades
    public static void mostrarMonuments(){
        Monument m = new Monument(null, null);
        ObjectSet result = db.queryByExample(m);
        
        while(result.hasNext()){
            Monument m2 = (Monument) result.next();
            System.out.println(m2);
        }
    }
    
    //mostra les comarques amb monument dun selge en especific
    public static void mostrarComarquesSegle(String selge){
        
        Comarca c = new Comarca(null, null, '\0', 0, new Monument(null, selge));
        ObjectSet result = db.queryByExample(c); 
        
        while(result.hasNext()){
            Comarca c2 = (Comarca) result.next();
            System.out.println(c2);
        }
        
    }
    
    
    
    
}
