/*
 * Classe comarca per treballar amb objectes complexos
 */
package comarques;

/**
 *
 * @author Ferran
 */
public class Comarca {
    private String nom; 
    private String capital; 
    private char provincia;
    private int altitut;
    private Monument monument;
    
    //Constructors
    
    public Comarca(){}

    public Comarca(String nom, String capital, char provincia, int altitut, Monument monument) {
        this.nom = nom;
        this.capital = capital;
        this.provincia = provincia;
        this.altitut = altitut;
        this.monument = monument;
    }

    
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public char getProvincia() {
        return provincia;
    }

    public void setProvincia(char provincia) {
        this.provincia = provincia;
    }

    public void setMonument(Monument monument) {
        this.monument = monument;
    }

    public Monument getMonument() {
        return monument;
    }

    public void setAltitut(int altitut) {
        this.altitut = altitut;
    }
    
    
    
    

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(); 
        sb.append(nom).append(", ").append(capital).append(", ").append(provincia)
                .append(" ").append(" altitud: ").append(altitut).append(" "); 
        
        if(monument != null){
            sb.append(monument.toString());
        }
        return sb.toString();
    }
    
    
    
    
}
