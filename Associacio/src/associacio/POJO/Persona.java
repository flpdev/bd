/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package associacio.POJO;

/**
 *
 * @author Ferran
 */
public class Persona {
    protected String nom;
    protected String cognom;
    protected String NIF;
    
    public Persona(String nom, String cognom, String NIF){
        this.nom = nom;
        this.cognom = cognom;
        this.NIF = NIF;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();

        sb.append("Persona -->  ").append("Nom: ").append(nom).append(" Cognom: ").append(cognom).append(" NIF: ").append(NIF);

        return sb.toString();
    }

    public String getNom() {
        return nom;
    }

    public String getCognom() {
        return cognom;
    }

    public String getNIF() {
        return NIF;
    }
    
    
}
