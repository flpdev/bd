/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package associacio.POJO;

/**
 *
 * @author Ferran
 */
public class Soci extends Persona{
    private int quota;

    public Soci(int quota, String nom, String cognom, String NIF) {
        super(nom, cognom, NIF);
        this.quota = quota;
    }
    
    public Soci(Persona persona, int quota) {
        super(persona.getNom(), persona.getCognom(), persona.getNIF());
        this.quota = quota;
    }
}
