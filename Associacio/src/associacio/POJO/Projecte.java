/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package associacio.POJO;

import com.db4o.collections.ArrayList4;

/**
 *
 * @author Ferran
 */
public class Projecte {
    private String nom;
    private Coordinador coordinador;
    private ArrayList4<Soci> socis;
    
    public Projecte(Coordinador c, String nom){
        this.coordinador = c;
        this.nom = nom;
        this.socis = new ArrayList4<>();
    }
    
    public void afegirSoci(Soci soci){
        this.socis.add(soci);
    }
}
