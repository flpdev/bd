/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package associacio;

import associacio2.BaseDades;
import associacio.POJO.Persona;
import associacio.POJO.Soci;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Ferran
 */
public class Associacio {

    /**
     * @param args the command line arguments
     */ 

    private static BaseDades db;

    public static void main(String[] args) {
        
        int opcio; 

        do{
            opcio = menu();
            
            db = new BaseDades("Associacio.db");

            switch(opcio){
                case 1:
                    mostrarPersones();
                    break;
                    
                case 3:
                    nouSoci();
                    break;
                    

            }
            db.closeDB();
        }while(opcio != 0);
        
    
    }
    
    /*Funcio menu, que no rep cap parametre i retorna un int, 
     en funcio de la opcio escollida per el usuari */
    public static int menu(){
        int opcio = -1; 
        
        System.out.println("1.- Veure persones");
        System.out.println("2.- Ceure projectes");
        System.out.println("3.- Nou soci");
        System.out.println("4.- Promoció soci");
        System.out.println("5.- Nou projecte");
        System.out.println("6.- Eliminar persona");
        System.out.println("7.- Eliminar projecte");
        System.out.println("8.- Modificar projecte");
        System.out.println("0.- Sortir");
        System.out.println("Opcio: ");
        opcio = new Scanner(System.in).nextInt();
        
        return opcio;
    }



    public static void mostrarPersones(){
        ArrayList<Persona> persones = new ArrayList<>();

        persones = db.getPersones();
        
        if(persones.size() == 0){
            System.out.println("No hi ha cap persona al sistema");
        }else{
            
            for (Persona p : persones) {
                System.out.println(p);
            }
            
        }
        
    }

    public static void nouSoci(){
        
        System.out.println("------- NOU SOCI --------");
        String nom = demanarNom();
        String cognom = demanarCognom();
        String NIF = demanarNIF();
        int quota = demanarQuota();

        db.insertSoci(new Soci(quota, nom, cognom, NIF));
        
    }


    //FUNCIONS PER DEMANAR DADES;

    public static String demanarNom(){

        String nom;

        do{
            System.out.println("Nom: ");
            nom = new Scanner(System.in).nextLine();

            if(nom.equals("")){
                System.out.println("S'ha d'introduir un nom");
            }
        }while(nom.equals(""));

        return nom;
    }

    public static String demanarCognom(){

        String cognom;

        do{
            System.out.println("Cognom: ");
            cognom = new Scanner(System.in).nextLine();

            
            if(cognom.equals("")){
                System.out.println("S'ha d'introduir un cognom");
            }
        }while(cognom.equals(""));

        return cognom;
    }

    public static String demanarNIF(){

        String NIF;

        do{
            System.out.println("NIF: ");
            NIF = new Scanner(System.in).nextLine();

            
            if(!validarNIF(NIF)){
                System.out.println("S'ha d'introduir un NIF correcte");
            }
        }while(!validarNIF(NIF));

        return NIF;
    }

    public static int demanarQuota(){

        int quota;

        do{
            System.out.println("Quota: ");
            quota = new Scanner(System.in).nextInt();

            
            if(quota < 0){
                System.out.println("S'ha d'introduir un Quota valida");
            }

        }while(quota < 0);

        return quota;
    }
    
    
    public static boolean validarNIF(String nif) {

        //http://www.interior.gob.es/web/servicios-al-ciudadano/dni/calculo-del-digito-de-control-del-nif-nie

        boolean NIFCorrecte = false;
        boolean vuit_numeros = false;
        char[] charArray = nif.toCharArray();
        
        if(charArray.length != 9){
            for(int i =0; i<charArray.length-1; i++){
                if(charArray[i] >= 48 || charArray[i] <= 57){
                    vuit_numeros = true;
                }else{
                    vuit_numeros=false;
                }
            }
            
            if(vuit_numeros){
                char lletra = nif.charAt(nif.length()-1);
                int numero = Integer.parseInt(nif.substring(0, nif.length()-2));
                if(lletra == calcularLletraNIF(numero)){
                   NIFCorrecte=true; 
                }
                
            }
        }
        
        return NIFCorrecte;
    }  
    
    public static char calcularLletraNIF(int nif){
        HashMap<Integer,Character> mapalletres = new HashMap<Integer,Character>();
        String lletres = "TRWAGMYFPDXBNJZSQVHLCKE";
        char[] lletresChar = lletres.toCharArray();
        
        for(int i = 0; i<23; i++){
            mapalletres.put(i, lletresChar[i]);
        }
        
        int resto = nif % 23;
        
        char lletra = mapalletres.get(resto);
        
        return lletra;
    }
    
    
}
