/**
 * Classe Llibre.java
 * Aquesta classe conte els atributs i metodes necessaris per 
 * els objectes de tipus llibre.
 * Data de creació: 15/12/2018
 * @author Ferran
 */

//paquet
package llibres;


public class Llibre {
    
    //atributs
    private String autor; 
    private String titol;
    private String editorial; 
    private String ciutat; 
    private int any; 

    
    //Constructor
    public Llibre(String autor, String titol, String editorial, String ciutat, int any) {
        this.autor = autor;
        this.titol = titol;
        this.editorial = editorial;
        this.ciutat = ciutat;
        this.any = any;
    }
    
    
    //Setters i getters

    public String getAutor() {
        return autor;
    }

    public String getTitol() {
        return titol;
    }

    public String getEditorial() {
        return editorial;
    }

    public String getCiutat() {
        return ciutat;
    }

    public int getAny() {
        return any;
    }
    

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public void setCiutat(String ciutat) {
        this.ciutat = ciutat;
    }

    public void setAny(int any) {
        this.any = any;
    }
    
    
    
    
}
