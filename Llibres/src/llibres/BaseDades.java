/**
 * Classe BaseDades.java
 * Aquesta classe ens permetra el us de una base de dades
 * DB4O per la gestió dels nostres llibres
 * Data de cració: 15/12/2018
 * @author Ferran
 */


//paquet
package llibres;

//imports
import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.util.ArrayList;
import java.util.Comparator;


public class BaseDades {
    
    private ObjectContainer db;
    
    //constructor
    public BaseDades(String nomArxiu){
        //Crea el arxiu llibres.db si no existeix, i l'obra si ja existeix
        this.db = Db4oEmbedded.openFile(nomArxiu);
    }
    
    
    //metodes
    
    /*
    * Funcio existeiguardarLlibrexLibre, 
    * Rep un objecte de tipus Llibre com a parametre. 
    * Guarda el objecte a la base de dades. 
    */
    public void guardarLlibre(Llibre llibre){
        db.store(llibre);
    }
    
    
    
    /*
    * Funcio existeixLibre, 
    * rep com a parametres dos Strings, el autor i el titol del llibre. 
    * Realitza un queryByExample i retorna un boolea depenent si ha trobat el llibre o no. 
    */
    public boolean existeixLlibre(String autor, String títol){
        boolean existeix = false; 
        
        //Query by example
        Llibre llibre = new Llibre(autor, títol, null, null, 0);
        ObjectSet result = this.db.queryByExample(llibre);
        
        if(result.size() > 0){ //Si existeix, variable existeix = true, sino es queda en false
            existeix = true;
        }
        
        return existeix;
    }
    
    
    /*
    * Funcio getLlibre, 
    * rep com a parametres dos Strings, el autor i el titol del llibre. 
    * Realitza un queryByExample i retorna un objecte de tipus llibre.
    * Si no troba el llibre a la base de dades, el objecte retornat sera null
    */
    public Llibre getLlibre(String autor, String titol){
        Llibre llibre = null; //Inicialitzat a null, si no es troba el llibre, es retornara un objecte null
        
        //QueryByExample
        Llibre llibreQuery = new Llibre(autor, titol, null, null, 0); 
        ObjectSet result = this.db.queryByExample(llibreQuery); 
        
        //Si hi ha resultats, es guarden en el objecte llibre
        if(result.size() > 0){
            llibre = (Llibre) result.next();
        }
        
        return llibre;
    }
    
    
    /*
    * Funcio getLlibres, 
    * No rep cap parametre. 
    * Retorna tots els llibres guardats a la base de dades.
    * Si no troba cap llibre retornara un arrayList buit
    */
    public ArrayList<Llibre> getLlibres(){
        ArrayList<Llibre> llibres = new ArrayList<Llibre>();
        
        //QueryByExample
        Llibre llibre = new Llibre(null, null, null, null, 0); 
        ObjectSet result = this.db.queryByExample(llibre);
        
        //Si hi ha resultats, es guarden un per un en 'llibres'
        if(result.size() > 0){
            for(int i =0; i< result.size(); i++){
                llibres.add((Llibre)result.next());
            }
            this.ordenarLlibres(llibres); 
        }
                
        return llibres;
    } 
    
    
    /*
    * Funcio getLlibres, 
    * Rep un string com a parametre, el autor. 
    * Retorna tots els llibres guardats a la base de dades del autor corresponent.
    * Si no troba cap llibre retornara un arrayList buit
    */
    public ArrayList<Llibre> getLlibres(String autor){
        ArrayList<Llibre> llibres = new ArrayList<Llibre>();
        
        //QueryByExample
        Llibre llibre = new Llibre(autor, null, null, null, 0); 
        ObjectSet result = this.db.queryByExample(llibre); 
        
        //Si hi ha resultats, es guarden un per un en 'llibres'
        if(result.size() > 0){
            for(int i=0; i<result.size(); i++){
                llibres.add((Llibre) result.next());
            }
            this.ordenarLlibres(llibres);
        }

        return llibres;
    } 
    
    
    /*
    * Funcio getLlibres, 
    * Rep un int com a parametre, el any. 
    * Retorna tots els llibres guardats a la base de dades del any corresponent.
    * Si no troba cap llibre retornara un arrayList buit
    */
    public ArrayList<Llibre> getLlibres(int any){
        ArrayList<Llibre> llibres = new ArrayList<Llibre>();
        
        //QueryByExample
        Llibre llibre = new Llibre(null, null, null, null, any);
        ObjectSet result = this.db.queryByExample(llibre); 
        
        //Si hi ha resultats, es guarden un per un en 'llibres'
        if(result.size() > 0){
            for(int i=0; i<result.size(); i++){
                llibres.add((Llibre)result.next());
            }
            this.ordenarLlibres(llibres);
        }
        
        return llibres;
    }

    
    /*
    * Funcio eliminarLlibre, 
    * Rep un objecte de tipus Llibre com a parametre. 
    * Busca i elimina el llibre de la base de dades.
    */
    public void eliminarLlibre(Llibre llibre){
        
        //QueryByExample
        ObjectSet result = this.db.queryByExample(llibre); 
        
        //Un cop trobat el llibre, s'elimina de la base de dades
        if(result.size() > 0){
            Llibre llibreTrobat = (Llibre) result.next();
            this.db.delete(llibreTrobat);
        }
    }
    
    
    /*
    * Funcio modificarLlibre, 
    * Rep quatre strings i un int com a parametre(autor, titol, editorial, ciutat, any). 
    * Busca i modifica el llibre a la base de dades.
    * Retorna un boolea segons si s'ha modificat el llibre o no.
    */
    public boolean modificarLlibre(String autor, String titol, String editorial, String ciutat, int any){
        boolean modificat = false;
        
        if(this.existeixLlibre(autor, titol)){
            Llibre llibre = this.getLlibre(autor, titol);
                 
            //Comprobacions del input del usuari i modificacio del llibre
            if(!editorial.equals("")){
                llibre.setEditorial(editorial);
            }
            if(!ciutat.equals("")){
                llibre.setCiutat(ciutat);
            }
            if(any != 0){
                llibre.setAny(any);
            }
            
            //tornem a guardar el llibrea a la bd
            this.db.store(llibre);
            modificat = true;
            
        }
        
        return modificat;
    }
    
    
    /*
    * Funcio ordenarPerAutor, 
    * Rep un arrayList de llibres i el ordena alfabeticament per el atribut autor, i despres per el atribut titol. 
    * No retorna res
    */
    public void ordenarLlibres(ArrayList<Llibre> llibres){
        llibres.sort(Comparator.comparing(Llibre::getAutor).thenComparing(Llibre::getTitol));
    }
    
    
   
    
    /*
    * Funcio closeDB, 
    * No rep cap parametre ni retorna res. 
    * Tanca la base de dades.
    */
    public void closeDB(){
        this.db.close();
    }
}
