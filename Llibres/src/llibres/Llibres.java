
/**
 * Aplicació llibres que permet la gestio de una base de dades DB4O de llibres
 * Data de creacio: 15/12/2018
 * @author Ferran López
 */

//paquet
package llibres;

//imports
import java.util.ArrayList;
import java.util.Scanner;


public class Llibres {

    //Declaracio de variables
    private static BaseDades baseDades;
    private static ArrayList<Llibre> llibres = new ArrayList<>();
    private static Llibre llibre;
    private static String autor; 
    private static String titol; 
    private static String editorial; 
    private static String ciutat; 
    private static int any;
    
    
    /*
    * Funció main.
    * Es la funció principal del programa, i des d'on es cridaran les respectives funcions
    * segons la opcio escollida per el usuari
    */
    public static void main(String[] args) {
        
        int opcio; 
        
        do{
            opcio = menu();
            
            //Cada iteracio obrim la BD, ja que si s'ha executat alguna opcio anteriorment
            //la BD estara tancada per no perdre informacio.
            baseDades = new BaseDades("llibres.db");
            
            switch(opcio){ 
                //En cada opcio, al acabar la accio solicitada per el usuari, es tanca la bd per guardar 
                //els canvis i que no es perdi informacio en cas que el programa es tanqui inesperadament.
                   
                case 1: //Mostrar tots els llibres
                    
                    mostrarTotsLlibres();
                    baseDades.closeDB();
                    break;
                    
                case 2: //Mostrar llibres segons el autor
                    
                    mostrarLlibresAutor();
                    baseDades.closeDB();
                    break;
                    
                case 3: //Mostrar llibres de un any
                    
                    mostrarLlibresAny();
                    baseDades.closeDB();
                    break;
                
                case 4: //Mostrar totes les dades de un llibre
                    
                    mostrarLlibre();
                    baseDades.closeDB();
                    break;
                  
                case 5: //Introduir llibre a la BD
                    
                    introduirLlibre();
                    baseDades.closeDB();
                    break;
                    
                case 6: //Modificar llibre
                    
                    modificarLlibre();
                    baseDades.closeDB();
                    break;
                    
                case 7: //Eliminar Llibre
                    
                    eliminarLlibre();
                    baseDades.closeDB();
                    break;
                       
                case 0: //Sortir
                    
                    baseDades.closeDB();
                    break;
                    
                default: //Si la opcio es incorrecte, tanquem la BD.
                    
                    baseDades.closeDB();
                    break;             
            }
            
        }while(opcio != 0);
    }
    
    
    /*Funcio menu, que no rep cap parametre i retorna un int, 
     en funcio de la opcio escollida per el usuari */
    public static int menu(){
        int opcio = -1; 
        
        System.out.println("1.- Mostrar tots els llibres");
        System.out.println("2.- Mostrar llibres d'un autor");
        System.out.println("3.- Mostrar llibres de un any");
        System.out.println("4.- Mostrar totes les dades de un llibre");
        System.out.println("5.- Introduir un llibre");
        System.out.println("6.- Modificar un llibre");
        System.out.println("7.- Eliminar un llibre");
        System.out.println("0.- Sortir");
        System.out.println("Opcio: ");
        opcio = new Scanner(System.in).nextInt();
        
        return opcio;
    }
    
    
    /*
    * Funcio demanarAutor.
    * No rep cap parametre
    * Retorna un String amb el nom del autor introduit per el usuari
    */
    public static String demanarAutor(){
        String autor;
        do{
            System.out.println("autor: ");
            autor = new Scanner(System.in).nextLine();
            if(autor.equals("")){
                System.out.println("S'ha d'introduir un autor");
            }
        }while(autor.equals(""));
        
        return autor;
    }
    
    
    /*
    * Funcio demanarTitol.
    * No rep cap parametre
    * Retorna un String amb el nom del titol introduit per el usuari
    */
    public static String demanarTitol(){
        String titol; 
        do{
            System.out.println("Titol: ");
            titol = new Scanner(System.in).nextLine();
            
            if(titol.equals("")){
                System.out.println("Introdueix un titol valid...");
            }
        }while(titol.equals(""));
        
        return titol;
    }
    
    
    /*
    * Funcio demanarAny.
    * No rep cap parametre
    * Retorna un int amb el any introduit per el usuari
    */
    public static int demanarAny(){
        int any; 
        do {            
            System.out.println("Any: ");
            any = new Scanner(System.in).nextInt();
            if(any<=0){
                System.out.println("Introdueix un any valid...");
            }
        } while (any <= 0);
            
        return any;
    }
    
    
    /*
    * Funcio demanarEditorial.
    * No rep cap parametre
    * Retorna un String amb la editorial introduida per el usuari
    */
    public static String demanarEditorial(){
        String editorial; 
        
        System.out.println("Editorial: ");
        editorial = new Scanner(System.in).nextLine();
        
        return editorial;
    }
    
    /*
    * Funcio demanarCiutat.
    * No rep cap parametre
    * Retorna un String amb la ciutat introduida per el usuari
    */
    public static String demanarCiutat(){
        String ciutat; 
        
        System.out.println("Ciutat: ");
        ciutat = new Scanner(System.in).nextLine();
        
        return ciutat;
    }
    
    
    /*
    * Funcio mostrarLlibre.
    * Rep un objecte Llibre per parametre
    * Mostra per consola el autor i titol del llibre rebut per parametre
    */
    public static void mostrarLlibre(Llibre llibre){
       StringBuilder sb = new StringBuilder();
       
       sb.append(llibre.getAutor()).append(": ").append(llibre.getTitol()); 
       System.out.println(sb.toString()); 
    }
    
    /*
    * Funcio mostrarLlibreSencer.
    * Rep un objecte Llibre per parametre
    * Mostra per consola totes les dades del llibre rebut com parametre
    */
    public static void mostrarLlibreSencer(Llibre llibre){
        StringBuilder sb = new StringBuilder();
        System.out.println("Dades del llibre: \n");
        sb.append("Autor: ").append(llibre.getAutor()).append("\nTitol: ")
                .append(llibre.getTitol()).append("\nEditorial: ")
                .append(llibre.getEditorial()).append("\nCiutat: ")
                .append(llibre.getCiutat()).append("\nAny: ")
                .append(llibre.getAny()).append("\n");
        System.out.println(sb.toString());
    }
    
    /*
    * Funcio mostrarTotsLlibres.
    * No rep cap parametre
    * Mostra per consola tots els llibres disponibles a la base de dades
    */
    public static void mostrarTotsLlibres(){
        
        llibres = baseDades.getLlibres(); //obtenim els llibres de la BD
        
        if(llibres.size()>0){
            System.out.println("Llibres: \n");
            for(int i=0; i<llibres.size(); i++){
                mostrarLlibre(llibres.get(i));
            }
        }else{
            System.out.println("No hi han llibres a la base de dades");
        }
    }
    
    
    /*
    * Funcio mostrarLlibresAutor.
    * No rep cap parametre
    * Mostra per consola tots els llibres disponibles a la base de dades de un autor
    */
    public static void mostrarLlibresAutor(){
        
        String autor = demanarAutor();
                    
        llibres = baseDades.getLlibres(autor);
        if(llibres.size()>0){
            System.out.println("Llibres de " + autor +": \n");
            for(int i=0; i<llibres.size(); i++){
                mostrarLlibre(llibres.get(i));
            }
        }else{
            System.out.println("No hi han llibres a la base de dades");
        }
    }
    
    
    /*
    * Funcio mostrarLlibresAny.
    * No rep cap parametre
    * Mostra per consola tots els llibres disponibles a la base de dades de un any
    */
    public static void mostrarLlibresAny(){
        int any = demanarAny();
       
        llibres = baseDades.getLlibres(any);
        
        if(llibres.size()>0){
            System.out.println("Llibres de " + any +": \n");
            for(int i=0; i<llibres.size(); i++){
                mostrarLlibre(llibres.get(i));
            }
        }else{
            System.out.println("No hi han llibres a la base de dades");
        }
    }
    
    /*
    * Funcio mostrarLlibre.
    * No rep cap parametre
    * Mostra per consola tota la informacio de un llibre en concret
    */
    public static void mostrarLlibre(){
        autor = demanarAutor();
        titol = demanarTitol();

        llibre = baseDades.getLlibre(autor, titol);

        if(llibre != null){
            System.out.println("Llibre: \n");
            mostrarLlibreSencer(llibre);
        }else{
            System.out.println("No s'ha trobat aquest llibre a la base de dades");
        }
    }
    
    /*
    * Funcio introduirLlibre.
    * No rep cap parametre
    * Demana els valors del nou llibre, mira si ja existeix, i si no existeix el guarda
    * a la base de dades, si existeix informa al usuari.
    */
    public static void introduirLlibre(){
       
        autor = demanarAutor();
        titol = demanarTitol();
        editorial = demanarEditorial();
        ciutat = demanarCiutat();
        any = demanarAny();
        
        
        
        if(!baseDades.existeixLlibre(autor, titol)){
            Llibre llibre = new Llibre(autor, titol, editorial, ciutat, any);
            baseDades.guardarLlibre(llibre);
            System.out.println("Llibre guardat a la base de dades");
            
        }else{
            System.out.println("Aquest llibre ja existeix");
        }
        
    }
    
    
    /*
    * Funcio modificarLlibre.
    * No rep cap parametre
    * Demana els valors del llibre a modificar.
    * Si no existeix s'informa al usuari
    */
    public static void modificarLlibre(){
        autor = demanarAutor();
        titol = demanarTitol();
        
        if(baseDades.existeixLlibre(autor, titol)){
            editorial = demanarEditorial();
            ciutat = demanarCiutat();
            System.out.println("any: ");
            any = new Scanner(System.in).nextInt(); //la funcio demanar any no contempla la opcio 0.
            
            baseDades.modificarLlibre(autor, titol, editorial, ciutat, any);
        }else{
            System.out.println("Aquest llibre no existeix");
        }
    }
    
    /*
    * Funcio eliminarLlibre.
    * No rep cap parametre
    * Demana el autor i titol del llibre a eliminar.
    * Demana confirmacio al usuari i elimina el llibre de la base de dades
    * Si no existeix s'informa al usuari
    */
    public static void eliminarLlibre(){
        autor = demanarAutor();
        titol = demanarTitol();
        
        if(baseDades.existeixLlibre(autor, titol)){
            Llibre llibre = baseDades.getLlibre(autor, titol);
            mostrarLlibreSencer(llibre);
            
            char opcio;
            do {                
                System.out.println("Estàs segur que vols eliminar el llibre (S/N): ");
                opcio = new Scanner(System.in).next().charAt(0);               
            } while (opcio != 'S' && opcio != 'N' && opcio !='s' && opcio != 'n');
            
            if(opcio == 'S' || opcio == 's'){
                baseDades.eliminarLlibre(llibre);
                System.out.println("Llibre eliminat de la base de dades");
            }else{
                System.out.println("Operacio cancelada per el usuari");
            }
        }else{
            System.out.println("Aquest llibre no existeix");
        }
        
        
        
    }
    
    
}
